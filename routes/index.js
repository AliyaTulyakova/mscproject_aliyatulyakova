var express = require('express');
var router = express.Router();

var Event = require('../routes/lib/Event');
var CalUser = require('../routes/lib/EventUsers');



router.get('/', function (req, res) {
      req.session.destroy();
       return res.render('index.ejs');

});


router.post('/calendar', function (req, res) {


    var title = req.body.title;
    var start = req.body.start;
    var end = req.body.end;
    var participant = req.body.participant;
    var allDay = req.body.allDay;

    var newevent = new Event();
    newevent.title = title;
    newevent.start = start;
    newevent.end = end;
    newevent.participant = participant;
    newevent.save(function (err, savedEvent) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        }

        return res.render('schedule.ejs');

    })

});

router.post('/addevent', function(req, res) {

    var title = req.body.title;
    var start = req.body.start;
    var startTime = req.body.startTime;
    var end = req.body.end;
    var endTime = req.body.endTime;

    var newevent = new Event();
    newevent.title = title;
    newevent.start = start;
    newevent.startTime = startTime;
    newevent.end = end;
    newevent.endTime = endTime;

    newevent.save(function (err, savedEvent) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        }

        return res.render('addevent.ejs');
    })

});


router.get('/eventsby', function(req, res) {
    if(!req.session.user){
     return res.status(401).send();
 }
    return res.render('events.ejs');
   

});

router.get('/allevents', function(req, res){
     var logvalue = req.headers['log'];
    if(logvalue && logvalue == 'info'){
        console.log("Request received for /allevents ");
    }
    var select = req.query.select;
    
    Event.find({}, function(err, foundData) {
        if(err) {
            console.log(err);
            res.status(500).send();
        } else{
            if(foundData.length == 0){
                var responseObject = undefined;
                if(select && select == 'count') {
                    responseObject = {count:0};
                }
                res.status(404).send(responseObject);
            } else{
                var responseObject = foundData;
                if(select && select == 'count') {
                    responseObject = {count: foundData.length};
                }
                res.send(responseObject);

            }
        }
   
    });
});

//Get Single Event
router.get('/event/:id', function(req, res){
    var id = req.params.id;
    Event.findOne({_id : id}, function(err, event){
       if(err){
           console.log(err);
           res.status(500).send();
       } 
        if(!event){
            return res.status(404).send();
        } else {
          
            return res.send(event);
        }
    });
    
});

router.get('/allusers', function(req, res) {
    var logvalue = req.headers['log'];
    if(logvalue && logvalue == 'info'){
        console.log("Request received for /allusers ");
    }
    var select = req.query.select;
    
    CalUser.find({}, function(err, foundData) {
        if(err) {
            console.log(err);
            res.status(500).send();
        } else{
            if(foundData.length == 0){
                var responseObject = undefined;
                if(select && select == 'count') {
                    responseObject = {count:0};
                }
                res.status(404).send(responseObject);
            } else{
                var responseObject = foundData;
                if(select && select == 'count') {
                    responseObject = {count: foundData.length};
                }
                res.send(responseObject);
            }
        }
   
    });
 
});

//Get Single user
router.get('/user/:id', function(req, res){
    var id = req.params.id;
    CalUser.findOne({_id : id}, function(err, user){
       if(err){
           console.log(err);
           res.status(500).send();
       } 
        if(!user){
            return res.status(404).send();
        } else {
            return res.send(user);
        }
    });
    
});


router.put('/update/:id', function(req, res){
   var id = req.params.id ;
    Event.findOne({_id : id}, function(err, foundObject){
        if(err) {
            console.log(err);
            res.status(500).send();
        } else{
            if(!foundObject){
                res.status(404).send();
            } else {
                if(req.body.title){
                 foundObject.title=req.body.title;
                }
                if(req.body.start) {
                    foundObject.start = req.body.start;
                }
                if(req.body.end) {
                    foundObject.end= req.body.end;
                }
                if(req.body.participant){
                    foundObject.participant = req.body.participant;
                }
            foundObject.save(function(err, updatedObject) {
                if(err) {
                    console.log(err);
                    res.status(500).send();
                } else{
                    res.send(updatedObject);
                }
            })
            }
        }
    });
});

router.put('/updateuser/:id', function(req, res) {
    var id= req.params.id;
    CalUser.findOne({_id : id}, function(err, foundObject){
        if(err){
            console.log(err);
            res.status(500).send();
        } else {
            if(req.body.username){
                foundObject.username = req.body.username;
            }
            if(req.body.email){
                foundObject.email = req.body.email;
            }
            if(req.body.password){
                foundObject.password = req.body.password;
            }
            
            foundObject.save(function(err, updatedObject){
                if(err){
                    console.log(err);
                    res.status(500).send();
                } else{
                    res.send(updatedObject);
                }
            })
        }
    });
});

router.delete('/deleteevent/:id', function(req, res){
 var id = req.params.id;
    Event.findOneAndRemove({_id : id}, function(err) {
       if(err){
           console.log(err);
           return res.status(500).send();
       } 
        return res.status(200).send();
    });
});

router.delete('/deleteuser/:id', function(req, res){
  var id = req.params.id;
    CalUser.findOneAndRemove({_id : id}, function(err){
        if(err){
            console.log(err);
            return res.status(500).send();
        }
        return res.status(200).send();
    });
});

router.post('/users', function(req, res) {
   var username = req.body.username;
    var email = req.body.email;
    var password = req.body.password;
    var pswdconfirm = req.body.pswdconfirm;
    
    var newuser = new CalUser();
    newuser.username = username;
    newuser.email = email;
    newuser.password = password;
    newuser.save(function (err, savedUser) {
        if(err){
            console.log(err);
            return res.status(500).send();
        }
        return res.status(200).send();
    })
});

router.post('/events', function (req, res) {
    var title = req.body.title;
    var start = req.body.start;
    var end = req.body.end;
    var participant = req.body.participant;
    var allDay = req.body.allDay;

    var newevent = new Event();
    newevent.title = title;
    newevent.start = start;
    newevent.end = end;
    newevent.participant = participant;
    newevent.save(function (err, savedEvent) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        }
        return res.status(200).send();
        return res.render('schedule.ejs');
    })
});

router.post('/signin', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;

    var url = "/eventsby/?username=";
    var fullurl = url + username;
    
    
    CalUser.findOne({
        username: username
    }, function (err, user) {
        if (err) {
            console.log(err);
            return req.status(500).send();
        }
        if (!user) {
            return res.status(404).send();

        }
        
        user.comparePassword(password, function(err, isMatch){
            if(isMatch && isMatch == true){
               req.session.user = user;
        return res.redirect(fullurl);
                
            } else {
               return res.status(401).send();
            }
        });
        

    })
});


router.get('/dashboard', function(req, res){
 if(!req.session.user){
     return res.status(401).send();
 }
    return res.status(200).send("Welcome to the web based personal organiser");
});

router.get('/signout', function(req, res){
    req.session.destroy();
    return res.status(200).send();
})

router.post('/welcome', function (req, res) {

    var username = req.body.username;
    var email = req.body.email;
    var password = req.body.password;

    var newuser = new CalUser();
    newuser.username = username;
    newuser.email = email;
    newuser.password = password;

    newuser.save(function (err, savedUser) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        }
        return res.redirect("/signin");
    })
})

module.exports = router;
