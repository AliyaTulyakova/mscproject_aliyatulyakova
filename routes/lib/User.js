var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/users');

var userSchema = new mongoose.Schema({
   username: {type: String, unique: true},
    email: {type: String},
    password: {type: String},
    pswconfirm: {type: String}
    
});

var User = mongoose.model('users', userSchema);
module.exports = User;
