var mongoose = require('mongoose');
//mongoose.connect('mongodb://localhost/calevents');
mongoose.connect('mongodb://aliya:australia@ds145193.mlab.com:45193/calevents');
var bcrypt = require('bcrypt-nodejs'),
    SALT_WORK_FACTOR = 10;

var eventSchema = new mongoose.Schema({
    title: {type: String, required: true},
    start: {type: Date, required: true},
    startTime: {type: String},
    end: {type: Date, required: true},
    endTime : {type: String},
    participant: {type: String}
});

/*
eventSchema.pre('save', function(next) {
    var event=this;

    //Hash the password if it has been modified or is new
    if(!event.isModified('title')) return next();

   
    
    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if(err) return next(err);

        //hash the password using a new salt
        bcrypt.hash(event.title, salt, null, function(err, hash){
            if(err) return next(err);

            //override the cleartext password with the hashed one
            event.title = hash;
            next();
        });



    });



});


eventSchema.methods.compareTitle = function(eventTitle, callback) {
    bcrypt.compare(eventTitle, this.title, function(err, isMatch){
        callback(undefined, isMatch);
    });
};  */

var Event = mongoose.model('calevents', eventSchema);
module.exports = Event;
