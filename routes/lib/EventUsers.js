var mongoose = require('mongoose');
//mongoose.connect('mongodb://localhost/calevents');
mongoose.connect('mongodb://aliya:australia@ds145193.mlab.com:45193/calevents');

var bcrypt = require('bcrypt-nodejs'),
    SALT_WORK_FACTOR = 10;


var caluserSchema = new mongoose.Schema({
   username: {type : String, unique: true, required: true},
   email: {type: String},
   password: {type: String},
   
});


caluserSchema.pre('save', function(next) {
var user = this;
    if(!user.isModified('password')) return next();
bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if(err) return next(err);
   bcrypt.hash(user.password, salt, null, function(err, hash){
       if(err) return next(err);

      user.password = hash;
       next();
   });

});
});

caluserSchema.methods.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch){
        if (err) return callback(err);
        callback(undefined, isMatch);
    });
};


var CalUser = mongoose.model('calusers', caluserSchema);
module.exports = CalUser;
