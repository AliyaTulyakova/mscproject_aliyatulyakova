app.controller('RegisterController', function ($scope, $http) {


    $scope.submit = function (req, res) {
        var username = $scope.username;
        var email = $scope.email;
        var password = $scope.password;

        var newuser = new CalUser();
        newuser.username = username;
        newuser.email = email;
        newuser.password = password;

        newuser.save(function (err, savedUser) {
            if (err) {
                console.log(err);
                return res.status(500).send();
            }
            return res.status(200).send();
        });
    };

    $scope.submit();

});
