ScheduleApp.controller('CalendarController', ['$scope', '$http', 'uiCalendarConfig',  function ($scope, $http, uiCalendarConfig) {
    
    $scope.selectedEvent = null;
    var isFirstTime = true;
    
    /* configure calendar */
    $scope.uiConfig = {
        calendar: {
            height: 550,
            editable: true,
                                              
            eventColor: '#9773BC',
            header: {
                left: 'month agendaWeek agendaDay',
                center: 'title',
                right: 'today prev,next'

            },
            timezone: 'UTC',
            selectable: true,
            selectHelper: true,
            select: function(start, end){
                var start = moment(start).format('YYYY/MM/DD LT');
                var end = moment(end).format('YYYY/MM/DD LT');
                $scope.newevent = {
                    title: '',
                    start : start,
                    end : end
                }

                $scope.ShowModal();
            },
            eventClick: function (event, jsEvent, view) {
              $scope.selectedEvent = event;
                var start = moment(event.start).format('YYYY/MM/DD LT');
                var end = moment(event.end).format('YYYY/MM/DD LT');
                $scope.newevent = {
                    title: event.title,
                    start : start,
                    end : end
                }
                $scope.ShowModal();



            }, 
            dayClick: function (date, jsEvent, view) {

            //  $("#myModal").modal();


            },
            
          // eventClick: $scope.alertOnEventClick,
            eventAdd: $scope.savedEvent,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            //eventRender: $scope.eventRender,
        }
    };

   
    //This is the funation to show modal dialog
    $scope.ShowModal = function(){
                          $('#myEventEdit').modal();
     $scope.updatedEvent = function(req, res, data){
         $scope.newevent = data.event;
         $http({
             method: 'POST',
             url : '/addevent',
             data : $scope.newevent
         }).then(function(response){
             if(response.data.status){
                 populate();
             }
         })
     }
    };



/* alert on eventClick */
    $scope.alertOnEventClick = function (event, jsEvent, view) {
         $scope.alertMessage = (data.title + ' was clicked ');
        
    };

    
    /* alert on Drop */
    $scope.alertOnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {
        $scope.alertMessage = ('Event Dropped to make dayDelta ' + delta);
    };
    /* alert on Resize */
    $scope.alertOnResize = function (event, delta, revertFunc, jsEvent, ui, view) {
        $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };

    /* Render Tooltip */
    $scope.eventRender = function (event, element, view) {

        element.attr({
            'tooltip': event.title,
            'tooltip-append-to-body': true
        });
        $compile(element)($scope);
    };

    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function (sources, source) {
        var canAdd = 0;
        angular.forEach(sources, function (value, key) {
            if (sources[key] === source) {
                sources.splice(key, 1);
                canAdd = 1;
            }
        });
        if (canAdd === 0) {
            sources.push(source);
        }
    };


    // $scope.config = {
    //     calendar: {
    //         editable: true,
    //         header:{
    //             left: 'month basicWeek basicDay agendaWeek agendaDay',
    //             center: 'title',
    //             right: 'today prev,next'
    //         }
    //          //eventClick: $scope.alertEventOnClick,
    //         // eventDrop: $scope.alertOnDrop,
    //         // eventResize: $scope.alertOnResize
    //     }
    // };

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    
    $scope.events = [
        
       /* {
            title: 'Appointment2\nImportant',
            start: new Date(y, m, d + 2, 8, 30),
            end: new Date(y, m, d + 2, 16, 30),
            allDay: false
        },
        
        {
            title: 'Appointment3',
            start: new Date(y, m, d + 3, 14, 30),
            end: new Date(y, m, d + 3, 22, 30),
            allDay: false
        },
        {
            title: 'Appointment4',
            start: new Date(y, m, d + 4, 22, 30),
            end: new Date(y, m, d + 4, 06, 30),
            allDay: false
        }*/
       
   ];
    
    $scope.myevents = [
    
    ];

    /* event sources array*/
    $scope.eventSources = [$scope.events];
    
    $scope.newevent = {};
    //this function gets datetime from json date
    function getDate(datetime){
        if(datetime !=null){
            var mili = datetime.replace(/\/Date\((-?\d+)\)\//, '$1');
            return new Date(parseInt(mili));
        }
        else {
            return "";
        }
    }
   
   //Load events from server
    function populate(){
    $http.get('https://api.mlab.com/api/1/databases/calevents/collections/calevents?apiKey=jgvg9NtGrNNMY0AhwAq9qz5GPOzFzKGb', {
       cache: false,
       params: {}
    }).then(function(data){
        $scope.events.slice(1, $scope.events.length);
        angular.forEach(data.data, function (value){
           $scope.events.push({
              title: value.title,
           // start: moment(value.start).format(),
           //    end: moment(value.end).format()
               start: moment(value.start.$date).format(),
               end: moment(value.end.$date).format()
           }); 
        });
    }); 
    }
    populate();
    $scope.clicked = function () {
        $scope.alertMessage = (' Button was clicked! ');

        /*
        $scope.events.push({
            title: '',
            start: new Date(y, m, d + 5, 22, 30),
            end: new Date(y, m, d + 5, 06, 30),
            allDay: false
        }); */
        /*
        $scope.events.push({
            title: 'Click Appointment',
            start: new Date(y, m, d + 5, 22, 30),
            end: new Date(y, m, d + 5, 06, 30),
            allDay: false
        });  */
    };

    $scope.savedEvent = function (req, res) {

      var eventTitle = document.getElementById("title").value;
        console.log(eventTitle);

            var eventStart = document.getElementById("start").value;
      var eventEnd = document.getElementById("end").value;


      $scope.events.push({
                        title: eventTitle,
                        start: eventStart,
                        end: eventEnd,
                        allDay: false
                    });

        var title = $scope.title;
        var start = $scope.start;
        var end = $scope.end;


        var newevent = new Event();
        newevent.title = title;
        newevent.start = start;
        newevent.end = end;
    
    
        newevent.save(function(err, savedEv){
              if(err){
            console.log(err);
            return res.status(500).send();
        }        
            return res.status(200).send();
                    });
        
      

    };


    $scope.removedEvent = function (title) {
        $scope.events.splice(title, 1);
    };

    // $scope.clicked = function(date){
    //   $scope.alertMessage = (' Button was clicked! '+date.title);
    // };


    /* alert on eventClick */
    $scope.alertOnEventClick = function (date, jsEvent, view) {
        $scope.alertMessage = (date.title + ' was clicked ');
    };
    // $scope.data = {
    //     events: [
    //         {
    //             title: 'An Event',
    //             start: new Date(y, m, d, 10, 0),
    //             end: new Date(y, m, d + 2, 12, 0)
    //         }
    //     ]
    // };
    //
    // $scope.data.eventSources = [$scope.data.events];
    //   $scope.events = [
    //    {title: 'Appointment1',start: new Date(y, m, d + 1, 6, 30),end: new Date(y, m, d + 1, 14, 30),allDay: false},
    //    {title: 'Appointment2\nImportant',start: new Date(y, m, d + 2, 8, 30),end: new Date(y, m, d + 2, 16, 30),allDay: false},
    //    {title: 'Appointment3',start: new Date(y, m, d + 3, 14, 30),end: new Date(y, m, d + 3, 22, 30),allDay: false},
    //    {title: 'Appointment4',start: new Date(y, m, d + 4, 22, 30),end: new Date(y, m, d + 4, 06, 30),allDay: false},
    //  ];
     /* event sources array*/
     // $scope.eventSources = [$scope.events];

}]);
