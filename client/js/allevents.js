var app = angular.module('myApp', ['ngRoute', 'ngResource'])

.service('ngRoute', 'ngResource', function(){})
app.controller('myCtrl',  ['$scope', '$http', function ($scope, $http) {


    $http.get("https://api.mlab.com/api/1/databases/calevents/collections/calevents?apiKey=jgvg9NtGrNNMY0AhwAq9qz5GPOzFzKGb").then(function (res) {
        
       
        $scope.events = res.data;

    });
    

     $scope.signedOut = function(req, res){
      req.session.destroy();
         return res.redirect('/');
     };
    
    $scope.savedEvent = function (req, res) {

        
        var title = $scope.title;
        var start = $scope.start;
        var end = $scope.end;


        var newevent = new Event();
        newevent.title = title;
        newevent.start = start;
        newevent.end = end;

        newevent.save(function (err, savedEv) {
            if (err) {
                console.log(err);
                return res.status(500).send();
            }
            return res.status(200).send();
        });

        $scope.events.push($scope.newevent);
    };
    
    

    $scope.selectedEvent = function (event) {
        console.log(event);
        $scope.clickedEvent = event;
    };


    $scope.updatedEvent = function () {
        $scope.message = "Event has been updated";
    };

    //Delete event
    $scope.delete = function (req, res) {
        $scope.events.splice($scope.events.indexOf(title), 1);
    };

    $scope.clearMessage = function () {
        $scope.message = "";
    }

    }]);
